package com.epam.geometric;

public abstract class Shape {
    private Color color;
    public Shape(Color color){
        this.color = color;
    }

    public Color getColor() {
        System.out.println("Color of geometric figure is: " + color);
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract double calculateArea();


}
