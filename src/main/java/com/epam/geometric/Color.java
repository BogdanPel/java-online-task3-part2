package com.epam.geometric;

public enum Color {
    RED,
    BLUE,
    GREEN,
    ORANGE,
    PURPLE,
    YELLOW,
    GRAY,
    NOCOLOR
}
