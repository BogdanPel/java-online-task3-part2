package com.epam.geometric;

public class Triangle extends Shape {
   private double a,b,c;
   private double p;
   private double calc;
    public Triangle(double a,double b,double c) {
        super(Color.NOCOLOR);
        this.a = a;
        this.b = b;
        this.c = c;
        this.p = (a+b+c)/2;
    }
    @Override
    public double calculateArea() {
        System.out.println("Area of your Triangle is: ");
    calc = p*(p-a)*(p-b)*(p-c);
        return Math.sqrt(calc);
    }
}
