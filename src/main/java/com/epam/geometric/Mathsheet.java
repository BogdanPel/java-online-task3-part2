package com.epam.geometric;

public class Mathsheet {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(24.0,30.0,18.0);
        System.out.println(triangle.calculateArea());
        Rectangle rectangle = new Rectangle(30.4,40.5);
        System.out.println(rectangle.calculateArea());
        rectangle.setColor(Color.RED);
        System.out.println(rectangle.getColor());
    }
}


