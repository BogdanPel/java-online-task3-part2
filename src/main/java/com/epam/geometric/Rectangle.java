package com.epam.geometric;

public class Rectangle extends Shape {
    private double a,b;
    public Rectangle (double a,double b){
        super(Color.RED);
        this.a = a;
        this.b = b;

    }

    @Override
    public double calculateArea() {
        System.out.println("Area of your Rectangle is: ");
        return a*b;
    }
}
