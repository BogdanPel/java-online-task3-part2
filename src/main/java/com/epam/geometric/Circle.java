package com.epam.geometric;

public class Circle extends Shape {
    private double radius;
    public Circle(double radius) {
        super(Color.NOCOLOR);
        this.radius = radius;

    }

    @Override
    public double calculateArea() {
        System.out.println("Area of your Circle is: " );
        return  Math.PI*Math.pow(radius,2);
    }
}
